const Throttle=(fn,delay=1000)=>{
    let wait = false
    let waitingArgs=null
    const timeOutFn=()=>{
      if(waitingArgs==null){
        wait=false
      }else{
        fn(...waitingArgs)
        waitingArgs=null
        setTimeout(timeOutFn,delay)
      }
    }
    return(...args)=>{
  
      if(wait){
        waitingArgs=args
        return
      }
      fn(...args)
      wait = true
      setTimeout(timeOutFn,delay)
    }
  }

  export default Throttle