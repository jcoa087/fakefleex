import { useState,useRef,useEffect } from 'react'
import manga from './assets/allay.jpg'
import card from './assets/card.jpg'
import './App.css'
import { BsChevronLeft,BsChevronRight } from "react-icons/bs";
import Throttle from './Throttle';
import Debounce from './Debounce';
function App() {

  const [showLeft,setShowLeft] = useState(false)
  const [movement,setMovement] = useState(0)
  const [bars,setBars] = useState([])
  const [slideContainers,setSlideContainers] = useState([])
  let ignore = false
  let slider = document.querySelector('.slider')
  const root = document.querySelector(':root')
  let images =[ 
 
    {url:card},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
    {url:manga},
  ]

  const showLeft_button = ()=>{
    setTimeout(() => {
      setShowLeft(true)
      root.style.setProperty('--child-opacity',1)
    }, 800); 
    
  }
  const right = ()=>{
    
    const itemsPerScreen = parseInt(getComputedStyle(root).getPropertyValue('--slide-container-quantity'))
    slider.style.transition = '1s'
    slider.style.marginLeft='-200%'
    setTimeout(()=>{
      
      slider.style.transition = 'none'
      slider.insertAdjacentElement('beforeend',slider.children[0])
      slider.style.marginLeft='-100%'
      movement === itemsPerScreen-1 ? setMovement(0): setMovement(movement+1)
      
    },900)

    showLeft_button()

  }
  //When you click left or right and wait just a little click right or lefta gain and you'll see that the slides moves but the bar for its slides does not coincide
  const move_right = Debounce(right,500)

  const left= ()=>{
    const itemsPerScreen = parseInt(getComputedStyle(root).getPropertyValue('--slide-container-quantity'))
    const last = slider.children.length-1

    slider.style.transition = '1s'
    slider.style.marginLeft='0%'
    setTimeout(()=>{
      slider.style.transition = 'none'
      slider.insertAdjacentElement('afterbegin',slider.children[last])
      slider.style.marginLeft='-100%'
      movement === 0  ? setMovement(itemsPerScreen-1): setMovement(movement-1)
    },800)
    
  }
  const move_left=Debounce(left,500)
  const get_bars = ()=>{
    
    let total = []
    const slider = document.querySelector('.slider')
    const itemsPerScreen = parseInt(getComputedStyle(slider).getPropertyValue('--items-on-screen'))
    const calcBars = Math.ceil(images.length / itemsPerScreen) 
    
    for (let index = 0; index < calcBars; index++) {
      let bar={}
      bar['index'] = index  
      if(total.length<calcBars)total.push(bar)
    }
    
    if(bars.length===0)
    {
      
      setBars([])
      setBars(bars=>bars.concat(total))
    }
    
  }
  const calc_slide_containers = ()=>{ 
    const slider = document.querySelector('.slider')
    const slider_containers = parseInt(document.querySelectorAll('.slides-container').length)
    const slider_columns = parseInt(getComputedStyle(slider).getPropertyValue('--items-on-screen'))
    const slider_rows= Math.ceil(images.length / slider_columns) 
    let slides_containers = Array(slider_rows).fill().map(()=>Array(slider_columns).fill())
    let img_col=0
    let img_cont=0
    
    for (let img_row = 0; img_row < slider_rows; img_row++) {
      while(img_col<slider_columns && img_cont < images.length ){
        images[img_cont]['index'] = img_col
        slides_containers[img_row][img_col] = images[img_cont]
        img_cont++
        img_col++
      }
      if(img_col===slider_columns)img_col=0
      
    }
   
    slider_containers !==0 ? root.style.setProperty('--slide-container-quantity',slider_containers) : ''
    if(slideContainers.length===0){
      
      setSlideContainers([])
      setSlideContainers(slideContainers=>slideContainers.concat(slides_containers))
    }
  }
  useEffect(()=>{
    
    if(!ignore){
      get_bars()
      calc_slide_containers()
    }
    return ()=>{ignore=true}
  },[])
  const recalc_bars = Throttle(get_bars)
  const recalc_slide_containers = Throttle(calc_slide_containers)
  window.addEventListener('resize',()=>{
    recalc_bars()
    recalc_slide_containers()
  })

  return (
    <div className='slider-main-container'>
      <div className="bars">
      {
        bars.map((bar,idx)=>
        <div key={idx} className={(movement) === bar.index ? 'bar active':'bar'} ></div>
        )
      }
      </div>
      <button onClick={move_right} className='btn right'><BsChevronRight className='icon' /></button>
      {showLeft ? <button onClick={move_left} className='btn left'><BsChevronLeft className='icon' /></button>:''}

      <div className="slider-container">
        <div className="slider" >
          {
            slideContainers.map((slideContainer,idx)=>
            <div className="slides-container" key={idx}> 
              {
                slideContainer.map((slide,index)=>
                slide ? <div className="slide" key={index}><img src={slide['url']} alt="" /></div> : ''
                )
              }
            </div>
            )
          }
        </div>
        
      </div>
      
    </div>
  )
}
// https://blog.bitsrc.io/react-v18-0-useeffect-bug-why-do-effects-run-twice-39babecede93
// https://javascript.plainenglish.io/how-to-add-to-an-array-in-react-state-3d08ddb2e1dc
export default App
