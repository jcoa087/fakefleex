const key = '02ed504857a7e095050a945b89ff6162'

const requests={
    requestPopular:`https://api.themoviedb.org/3/movie/popular?api_key=${key}&page=1`,
    requestTopRated:`https://api.themoviedb.org/3/movie/top_rated?api_key=${key}&page=1`,
    requestTrending:`https://api.themoviedb.org/3/movie/popular?api_key=${key}&page=2`,
    requestHorror:`https://api.themoviedb.org/3/movie/popular?api_key=${key}&query=horror&page=2`,
    requestUpcoming:`https://api.themoviedb.org/3/movie/upcoming?api_key=${key}&page=1`,
}

export default requests